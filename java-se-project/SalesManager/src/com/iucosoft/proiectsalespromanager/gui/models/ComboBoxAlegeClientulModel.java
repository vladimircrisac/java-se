/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.gui.models;

import com.iucosoft.proiectsalespromanager.entitati.Client;
import java.util.List;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author iucosoft
 */
public class ComboBoxAlegeClientulModel extends DefaultComboBoxModel<Client> {

    public ComboBoxAlegeClientulModel(List<Client> listaClienti) {

        Client cl = new Client();
        cl.setId(0);
        cl.setNumeClient("Selecteaza clientul");

        super.addElement(cl);

        for (Client client : listaClienti) {
            super.addElement(client);
        }

    }

}
