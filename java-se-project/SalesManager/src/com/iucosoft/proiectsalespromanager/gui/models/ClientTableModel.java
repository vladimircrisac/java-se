/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.gui.models;

import com.iucosoft.proiectsalespromanager.entitati.Client;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author iucosoft
 */
public class ClientTableModel extends DefaultTableModel {

    String[] columns = {"ID", "Nume client", "Adresa fizica", "Adresa juridica",
        "Director", "Telefon", "Email"};

    public ClientTableModel(List<Client> listaClienti) {
        for (String col : columns) {
            super.addColumn(col);
        }

        for (Client client : listaClienti) {
            Vector row = new Vector();

            row.addElement(client.getId());
            row.addElement(client.getNumeClient());
            row.addElement(client.getAdresaFizica());
            row.addElement(client.getAdresaJuridica());
            row.addElement(client.getDirector());
            row.addElement(client.getTel());
            row.addElement(client.getEmail());

            super.dataVector.add(row);
        }
    }
}
