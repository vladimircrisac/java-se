package com.iucosoft.proiectsalespromanager.entitati;

import java.io.Serializable;

public class Produs implements Serializable {

    private int id;
    private String numeProdus;
    private int barCode;
    private double pret;
    private int stock;
    private String producator; // denumirea producatorului
    private String comentariu;

    public Produs() {
    }

    public Produs(int id, String numeProdus, int barCode, double pret, int stock, String producator, String comentariu) {
        this.id = id;
        this.numeProdus = numeProdus;
        this.barCode = barCode;
        this.pret = pret;
        this.stock = stock;
        this.producator = producator;
        this.comentariu = comentariu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeProdus() {
        return numeProdus;
    }

    public void setNumeProdus(String numeProdus) {
        this.numeProdus = numeProdus;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    public double getPret() {
        return pret;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }

    public String getComentariu() {
        return comentariu;
    }

    public void setComentariu(String comentariu) {
        this.comentariu = comentariu;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        hash = 67 * hash + this.barCode;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produs other = (Produs) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.barCode != other.barCode) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Produs{" + "id=" + id + ", numeProdus=" + numeProdus + ", barCode=" + barCode + ", pret=" + pret + ", stock=" + stock + ", producator=" + producator + ", comentariu=" + comentariu + '}';
    }

}
