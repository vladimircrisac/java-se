package com.iucosoft.proiectsalespromanager.entitati;

import java.io.Serializable;

public class Client implements Serializable {

    private int id;
    private String numeClient;
    private String adresaFizica;
    private String adresaJuridica;
    private String director; // admin, manager - persoana responsabila
    private int tel;
    private String email;

    public Client() {
    }

    public Client(int id, String numeClient, String adresaFizica, String adresaJuridica, String director, int tel, String email) {
        this.id = id;
        this.numeClient = numeClient;
        this.adresaFizica = adresaFizica;
        this.adresaJuridica = adresaJuridica;
        this.director = director;
        this.tel = tel;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public String getAdresaFizica() {
        return adresaFizica;
    }

    public void setAdresaFizica(String adresaFizica) {
        this.adresaFizica = adresaFizica;
    }

    public String getAdresaJuridica() {
        return adresaJuridica;
    }

    public void setAdresaJuridica(String adresaJuridica) {
        this.adresaJuridica = adresaJuridica;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return numeClient;
    }

}
