/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.entitati;

import java.io.Serializable;

/**
 *
 * @author Denis
 */
public class ComandaNoua implements Serializable {

    private int id;
    private Produs produs;
    private Client client;
    private int cantitate;
    private int pret;

    public ComandaNoua() {
    }

    public ComandaNoua(int id, Produs produsNou, Client clientNou, int cantitate, int pret) {
        this.id = id;
        this.produs = produsNou;
        this.client = clientNou;
        this.cantitate = cantitate;
        this.pret = pret;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Produs getProdus() {
        return produs;
    }

    public void setProdus(Produs produs) {
        this.produs = produs;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    @Override
    public String toString() {
        return "ComandaNoua{" + "id=" + id + ", produsNou=" + produs + ", clientNou=" + client + ", cantitate=" + cantitate + ", pret=" + pret + '}';
    }

}
