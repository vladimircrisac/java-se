package com.iucosoft.proiectsalespromanager.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class MyDataSource {

    private static final Logger LOG = Logger.getLogger(MyDataSource.class.getName());

    private static MyDataSource instance;

    public static MyDataSource getInstance() {
        if (instance == null) {
            instance = new MyDataSource();
        }
        return instance;
    }

    private MyDataSource() {
        try {
            loadProperties();
            LOG.info("properties loaded succesfull");

            loadDrivers();
            LOG.info("drivers loaded succesfull");

            testConnection();
            LOG.info("connection ok. Connected!");

        } catch (Exception e) {
            LOG.severe(e.toString());
        }
    }

    private void loadProperties() {
        driverName = "com.mysql.jdbc.Driver";
        username = "root";
        password = "root";
        urldb = "jdbc:mysql://localhost:3306/salespromanager";
    }

    private void loadDrivers() throws ClassNotFoundException {
        Class.forName(driverName);
    }

    private void testConnection() throws SQLException {
        connection = DriverManager.getConnection(urldb, username, password);
        if (connection != null && connection.isClosed()) {
            Statement st = connection.createStatement();
            st.executeUpdate("CREATE TABLE if not exists t1 (id INT NOT NULL);");

            int v = 234;

            st.executeUpdate("insert into t1 (id) values(" + v + ") ");

            LOG.info("inserted text");

            ResultSet rs = st.executeQuery("SELECT * FROM t1 where id=" + v);
            if (rs.next()) {
                int vv = rs.getInt(1);
                if (v == vv) {
                    LOG.info("select test OK");
                }
            }
            st.executeUpdate("drop table t1");
            LOG.info("drop test ok");

        }

    }

    private void createConnection() throws SQLException {
        if (connection == null || (connection != null && connection.isClosed())) {
            connection = DriverManager.getConnection(urldb, username, password);
        }
    }

    String driverName;
    String username;
    String password;
    String urldb;

    private Connection connection;

    public Connection getConnection() throws SQLException {
        createConnection();
        return connection;
    }

    public static void main(String[] args) {
        MyDataSource mds = MyDataSource.getInstance();
    }

}
