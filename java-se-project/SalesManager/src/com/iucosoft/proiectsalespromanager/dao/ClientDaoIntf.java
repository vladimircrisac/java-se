/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.dao;

import com.iucosoft.proiectsalespromanager.entitati.Client;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Denis
 */
public interface ClientDaoIntf {

    void save(Client client) throws SQLException; // done

    void update(Client client) throws SQLException; // edit - done

    void delete(Client client) throws SQLException; // done

    Client findByID(int id) throws SQLException; // done

    Client findByNumeClient(String numeClient) throws SQLException; //done

    Client findByAdresaFizica(String adresaFizica) throws SQLException; //done

    Client findByAdresaJuridica(String adresaJuridica) throws SQLException; //done

    Client findByDirector(String director) throws SQLException; //done

    Client findByNrTel(int nrTel) throws SQLException; //done

    Client findByEmail(String email) throws SQLException; //done

    List<Client> findAll() throws SQLException; // swoh all  - done
}
