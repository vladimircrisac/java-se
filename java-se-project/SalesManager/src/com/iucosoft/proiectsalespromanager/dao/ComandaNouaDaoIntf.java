/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.dao;

import com.iucosoft.proiectsalespromanager.entitati.Client;
import com.iucosoft.proiectsalespromanager.entitati.ComandaNoua;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author iucosoft
 */
public interface ComandaNouaDaoIntf {

    void save(ComandaNoua comandaNoua) throws SQLException;

    void delete(ComandaNoua comandaNoua) throws SQLException;

    List<ComandaNoua> findAllProduse() throws SQLException;

    List<ComandaNoua> adaugaInCos() throws SQLException;

}
