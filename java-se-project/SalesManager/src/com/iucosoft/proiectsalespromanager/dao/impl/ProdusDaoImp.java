/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.dao.impl;

import com.iucosoft.proiectsalespromanager.dao.ClientDaoIntf;
import com.iucosoft.proiectsalespromanager.dao.ProdusDaoIntf;
import com.iucosoft.proiectsalespromanager.db.MyDataSource;
import com.iucosoft.proiectsalespromanager.entitati.Produs;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Denis
 */
public class ProdusDaoImp implements ProdusDaoIntf {

    private static final Logger LOG = Logger.getLogger(ProdusDaoIntf.class.getName());

    private MyDataSource mds = MyDataSource.getInstance();
    private Connection connection;

    @Override
    public void save(Produs produs) throws SQLException {
// INSERT INTO `salespromanager`.`produse` (`NUME_PRODUS`, `BARCODE`, `PRET`, `STOCK`, `PRODUCATOR`, `COMENTARIU`) VALUES ('produs7', '7777', '77', '70', 'producator2', 'com2');

        String sql = "INSERT INTO produse "
                + "(NUME_PRODUS, BARCODE,"
                + " PRET, STOCK,PRODUCATOR, COMENTARIU) "
                + "VALUES ("
                + "'" + produs.getNumeProdus() + "', "
                + produs.getBarCode() + ", "
                + produs.getPret() + ", "
                + produs.getStock() + ", "
                + "'" + produs.getProducator() + "', "
                + "'" + produs.getComentariu() + "');";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        stat.executeUpdate(sql);

        LOG.info("saved!");
        connection.close();

    }

    @Override
    public void update(Produs produs) throws SQLException {

        String sql = "UPDATE `salespromanager`.`produse` SET "
                + "`NUME_PRODUS`='" + produs.getNumeProdus() + "', "
                + "`BARCODE`='" + produs.getBarCode() + "', "
                + "`PRET`='" + produs.getPret() + "', "
                + "`STOCK`='" + produs.getStock() + "', "
                + "`PRODUCATOR`='" + produs.getProducator() + "', "
                + "`COMENTARIU`='" + produs.getComentariu() + "' "
                + "WHERE `ID_PRODUS`='" + produs.getId() + "';";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        stat.executeUpdate(sql);

        LOG.info("updated!");
        connection.close();
    }

    @Override
    public void delete(Produs produs) throws SQLException {
        String sql = "DELETE FROM `salespromanager`.`produse` "
                + "WHERE `ID_PRODUS`='" + produs.getId() + "';";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        stat.executeUpdate(sql);
        LOG.info("deleted!");
        connection.close();

    }

    @Override
    public Produs findByID(int id) throws SQLException {
        Produs produs = null;
        String sql = "SELECT * FROM `salespromanager`.`produse` "
                + "WHERE `ID_PRODUS` = '" + produs.getId() + "'";
        connection = mds.getConnection();
        Statement stat = connection.createStatement();

        ResultSet rs = stat.executeQuery(sql);
        if (rs.next()) {
            produs = new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7));
        }
        connection.close();
        return produs;
    }

    @Override
    public Produs findByNumeProdus(String numeProdus) throws SQLException {
        Produs produs = null;
        String sql = "SELECT * FROM `salespromanager`.`produse` "
                + "WHERE `NUME_PRODUS` = '" + produs.getNumeProdus() + "'";
        connection = mds.getConnection();
        Statement stat = connection.createStatement();

        ResultSet rs = stat.executeQuery(sql);
        if (rs.next()) {
            produs = new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7));
        }
        connection.close();
        return produs;
    }

    @Override
    public Produs findByBarcode(int barcode) throws SQLException {
        Produs produs = null;
        String sql = "SELECT * FROM salespromanager.produse where BARCODE =" + barcode;
        connection = mds.getConnection();
        Statement stat = connection.createStatement();

        ResultSet rs = stat.executeQuery(sql);
        if (rs.next()) {
            produs = new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7));
        }
        connection.close();
        return produs;
    }

    @Override
    public List<Produs> findByPret(double pret) throws SQLException {
        List<Produs> listaProduse = new ArrayList<>();

        String sql = "SELECT * FROM salespromanager.produse where PRET = " + pret;
        connection = mds.getConnection();
        Statement stat = connection.createStatement();

        ResultSet rs = stat.executeQuery(sql);
        while (rs.next()) {
            Produs produs = new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7));
            listaProduse.add(produs);
        }
        connection.close();
        return listaProduse;
    }

    @Override
    public List<Produs> findByProducator(String producator) throws SQLException {
        List<Produs> listaProduse = new ArrayList<>();

        String sql = "SELECT * FROM salespromanager.produse where PRODUCATOR = '" + producator + "'";
        connection = mds.getConnection();
        Statement stat = connection.createStatement();

        ResultSet rs = stat.executeQuery(sql);
        while (rs.next()) {
            Produs produs = new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7));
            listaProduse.add(produs);
        }
        connection.close();
        return listaProduse;
    }

    @Override
    public List<Produs> findByComentariu(String comentariu) throws SQLException {
        List<Produs> listaProduse = new ArrayList<>();

        String sql = "SELECT * FROM `salespromanager`.`produse` WHERE `COMENTARIU` = '" + comentariu + "'";
        connection = mds.getConnection();
        Statement stat = connection.createStatement();

        ResultSet rs = stat.executeQuery(sql);
        while (rs.next()) {
            Produs produs = new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7));
            listaProduse.add(produs);
        }
        connection.close();
        return listaProduse;
    }

    @Override
    public List<Produs> findAll() throws SQLException {
        List<Produs> listaProduse = new ArrayList<>();

        String sql = "SELECT * FROM `salespromanager`.`produse`";
        connection = mds.getConnection();
        Statement stat = connection.createStatement();

        ResultSet rs = stat.executeQuery(sql);
        while (rs.next()) {
            Produs produs = new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7));
            listaProduse.add(produs);
        }
        connection.close();
        return listaProduse;
    }

}
