package com.iucosoft.proiectsalespromanager.dao.impl;

import com.iucosoft.proiectsalespromanager.dao.ClientDaoIntf;
import com.iucosoft.proiectsalespromanager.entitati.Client;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestClienti {

    public static void main(String[] args) {
        
        try {
            Client client = new Client(0, "nc1", "af1", "aj1", "dir1", 1111, "email1");
            ClientDaoIntf clientDao = new ClientDaoImp();
            
            Client clientBD = clientDao.findByID(2);
            
            if (client.getNumeClient().equals(clientBD.getNumeClient())) {
                System.out.println(client);
            } 
        } catch (SQLException ex) {
            Logger.getLogger(TestClienti.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    
}
