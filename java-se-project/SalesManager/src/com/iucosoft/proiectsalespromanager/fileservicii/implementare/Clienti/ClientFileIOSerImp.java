/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.fileservicii.implementare.Clienti;

import com.iucosoft.proiectsalespromanager.entitati.Client;
import com.iucosoft.proiectsalespromanager.fileservicii.AbstractFileIOClient;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Denis
 */
public class ClientFileIOSerImp extends AbstractFileIOClient{
    
    @Override
    public void write(Collection<Client> coll, String fileDest) {
        try  (
                FileOutputStream fos = new FileOutputStream(fileDest);
                ObjectOutputStream oos = new ObjectOutputStream(fos);) {
            
            oos.writeObject(coll);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public Collection<Client> read(String fileSrc) {
        Collection<Client> collClient = new ArrayList<>();
        try(
        FileInputStream fis = new FileInputStream(fileSrc);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ){
            collClient = (Collection<Client>) ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClientFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return collClient;
    }
    
    public static void main(String[] args) {
        String fileName = "clienti.ser";
        
        ClientFileIOSerImp fileServiciu = new ClientFileIOSerImp();
        
        Collection<Client> coll = new ArrayList<>();
        coll.add(new Client(0, "numeClient1", "adresaFizica1", "adresaJuridica1", "director1", 1111, "email1"));
        coll.add(new Client(1, "numeClient2", "adresaFizica2", "adresaJuridica2", "director2", 2222, "email2"));
        
        System.out.println("scrie");
        fileServiciu.write(coll, fileName);
        System.out.println("scriere terminata in "+fileName+"\n");
        
        System.out.println("citire din fisier");
        Collection<Client> collClientCitit = fileServiciu.read(fileName);
        for (Client client : collClientCitit) {
            System.out.println("idClient = "+client.getId()+
                               " numeClient = "+client.getNumeClient()+
                               " adresaFizica = "+client.getAdresaFizica()+
                               " adresaJuridica = "+client.getAdresaJuridica()+
                               " director = "+client.getDirector()+
                               " nrTel  = "+client.getTel()+
                               " email  = "+client.getEmail());
        }
        System.out.println("citire finisata.");
   }
}
