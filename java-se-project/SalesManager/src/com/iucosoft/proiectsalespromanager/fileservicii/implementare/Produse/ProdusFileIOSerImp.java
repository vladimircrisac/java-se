package com.iucosoft.proiectsalespromanager.fileservicii.implementare.Produse;

import com.iucosoft.proiectsalespromanager.entitati.Produs;
import com.iucosoft.proiectsalespromanager.fileservicii.AbstractFileIOProdus;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProdusFileIOSerImp extends AbstractFileIOProdus {

    @Override
    public void write(Collection<Produs> collProdus, String fileDest) {
         
        try (
           FileOutputStream fos = new FileOutputStream(fileDest);
           ObjectOutputStream oos = new ObjectOutputStream(fos);
                ) {
           oos.writeObject(collProdus);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProdusFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProdusFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Collection<Produs> read(String fileSrc) {
        Collection<Produs> collProdus = new ArrayList<>();
        try (
            FileInputStream fis = new FileInputStream(fileSrc);
            ObjectInputStream ois = new ObjectInputStream(fis);
                ){
            collProdus = (Collection<Produs>) ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProdusFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProdusFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProdusFileIOSerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return collProdus;
    }

    public static void main(String[] args) {
        String fileName = "produs.ser";
        ProdusFileIOSerImp fileServiciu = new ProdusFileIOSerImp();
        
        Collection<Produs> collProd = new ArrayList<>();
        collProd.add(new Produs(0, "Produs1", 11111, 11, 100, "producator1", "comentariu1"));
        collProd.add(new Produs(1, "Produs2", 22222, 22, 200, "producator2", "comentariu2"));
        
        System.out.println("scrie in fisier: ");
        fileServiciu.write(collProd, fileName);
        System.out.println("scriere finisata in fisierul "+fileName+"\n");
        
        System.out.println("citire din fisier: "+fileName);
        for (Produs produs : collProd) {
            System.out.println("idProdus "+produs.getId()+
                               " numeProdus "+produs.getNumeProdus()+
                               " barcode "+produs.getBarCode()+
                               " pret "+produs.getPret()+
                               " stock "+produs.getStock()+
                               " producator "+produs.getProducator()+
                               " comentariu "+produs.getComentariu());
        }
        fileServiciu.read(fileName);
        System.out.println("citire finisata!");
        
    }

    
}
