/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.fileservicii;

import java.util.Collection;

/**
 *
 * @author iucosoft
 */
public interface MyReadable<T> {

    Collection<T> read(String fileSrc);
}
