Sales Manager.

Este aplicatie pentru comercializarea produselor. Destinata vanzatorului - doar adminul poate lucra in ea si face comenzi.

*** fisierul cu baza de date se afla in com.iucosoft.proiectsalespromanager.db si se numeste _sql.sql.

Are 4 ferestre: 

1. Produse: sunt disponibile functiile:

a) Show all - ne arata lista produselor disponibile;
b) find - cautarea produsului dupa barcode;
c) clear - sterge toate campurile;
d) save - salveaza produsul nou; sau un produs existent care este editat (ca atare asta trebuie sa o faca Edit-ul).
e) delete - sterge produsul din BD;
f) export - exporteaza datele din tabel si le salveaza intr-un fisier. (inca nu lucreaza).

2. Clienti - disponibile aceleasi functii ca si la Produse (functionalitatea e aceeasi), dar inca nu toate lucreaza.

3. Comanda noua.

3.1 Se selecteaza Clientul si Adresa de livrare din droplist.
3.2 Afiseaza produsele - avem lista de produse si stockul lor. 
3.2.1. Click pe produs -> Adauga in cos -> alegem cantitatea -> OK -> produsul este adaugat in Cos (tabelul din dreapta).

Functii: 

Clear - sterge campurile
Delete - sterge din Cos produsul selectat
Cumpara - adauga la Total numarul de produse si suma totala spre achitare. (la moment nu functioneaza cum trebuie).
Save - salveaza comanda in BD.

4. Lista comenzi.

** trebuie adaugate Clasele entitati necesare si tabelele in SQL. **

Functii:

Afiseaza - afiseaza in tabel lista de comenzi, care sunt in BD.
Deschide comanda - ne deschide comanda selectata in forma deschisa - se afiseaza produsele comandate, cantitatea si pretul.

----------------
Aici sa nu te sperii.
Aplicatia inca este foarte cruda. Nu stiu cand voi ajunge aici,dar va trebui sa ma apuc de ea, sa o finisez neaparat.